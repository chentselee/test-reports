import type { PageLoad } from './$types'

export const load: PageLoad = async ({ fetch }) => {
	const data = await fetch('https://gitlab.com/api/graphql', {
		method: 'POST',
		headers: {
			Authorization: `Bearer ${import.meta.env.VITE_GITLAB_TOKEN}`,
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			query: `
      query ($limit: Int, $cursor: String) {
  project(fullPath: "${import.meta.env.VITE_GITLAB_PROJECT_PATH}") {
    pipelines(first: $limit, after: $cursor) {
      pageInfo {
        endCursor
        hasNextPage
      }
      nodes {
        commit {
          message
        }
        createdAt
        testReportSummary {
          total {
            success
            skipped
            failed
            error
            time
            count
            suiteError
          }
          testSuites {
            nodes {
              name
              totalTime
              successCount
              skippedCount
              failedCount
              errorCount
              suiteError
            }
          }
        }
      }
    }
  }
}
      `,
			variables: { limit: 25 }
		})
	}).then(
		(res) =>
			res.json() as Promise<{
				data: {
					project: {
						pipelines: {
							nodes: any[]
						}
					}
				}
			}>
	)
	return {
		data,
		count: data.data.project.pipelines.nodes.reduce(
			(accumulator, current) => {
				return {
					count: accumulator.count + current.testReportSummary.total.count,
					success: accumulator.success + current.testReportSummary.total.success,
					failed: accumulator.failed + current.testReportSummary.total.failed,
					skipped: accumulator.skipped + current.testReportSummary.total.skipped
				}
			},
			{ count: 0, success: 0, skipped: 0, failed: 0 }
		)
	}
}

export const ssr = false
