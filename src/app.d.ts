// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
declare global {
	namespace App {
		// interface Error {}
		// interface Locals {}
		// interface PageData {}
		// interface Platform {}
	}
}

/// <reference types="vite/client" />

interface ImportMetaEnv {
	readonly VITE_GITLAB_TOKEN: string
	readonly VITE_GITLAB_PROJECT_PATH: string
}

interface ImportMeta {
	readonly env: ImportMetaEnv
}

export {}
